# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Hands-on practise

# +
# Load libraries
import pandas

import matplotlib.pyplot
import seaborn

import sklearn.preprocessing
import sklearn.decomposition
import sklearn.ensemble
# -

# Load data
clinical = pandas.read_csv('data/tcga_gbm_clinical.csv', index_col='bcr_patient_barcode')
rppa = pandas.read_csv('data/tcga_gbm_rppa.csv', index_col='bcr_patient_barcode')
data = clinical.join(rppa, how='inner')

# Combined clinical and RPPA data:
data.head()

# Explore clinical data
# Hint: experiment with different columns, and max, min, mean and median.
data.age.min()

# Explore RPPA data
# Hint: quantile function can find value for any quantile between 0 and 1 (0th and 100th percentile)
data.x1433epsilon.quantile(0.99)

# Play with plots. 
# Hint: try histograms (distribution plots) for some protein concentrations. Any interesting patterns?
# Hint2: you can can get the list of proteins available by typing rppa.columns
seaborn.distplot(data.age);

# Compare male and female age distribution in the dataset.
# Hint: Experiment with swarmplot, violinplot and boxplot
seaborn.swarmplot(x='sex', y='age', data=data);

# Can you find a protein that has different concentrations between male and females?
# Hint: replace the protein name with others of your choosing
seaborn.boxplot(x='sex', y='x1433epsilon', data=data);

# Regression plots. Can you find two proteins that correlate strongly?
seaborn.regplot(x='x1433epsilon', y='x4ebp1', data=data);

# +
# Let's standardize the protein expression

data_standardized = pandas.DataFrame(sklearn.preprocessing.StandardScaler().fit_transform(data[rppa.columns]),
                                     index=data.index,
                                     columns=rppa.columns)
# -

# Let's compute PCA for the standardized protein expression
rppa_pca = sklearn.decomposition.PCA(n_components=2).fit_transform(data_standardized[rppa.columns])
rppa.shape

# Let's plot the first 2 principal components
seaborn.scatterplot(x=rppa_pca[:,0], y=rppa_pca[:,1])
ax = matplotlib.pyplot.gca()
ax.set_xlabel('1st component')
ax.set_ylabel('2nd component');

# +
# How much variance do the first 2 components account for?
# Hint: look up the documentation at https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html#sklearn.decomposition.PCA
# -

# Train a random regressionforest to predict protein concentration based on age and sex:
forest = sklearn.ensemble.RandomForestRegressor(n_estimators=100, oob_score=True)
forest.fit(pandas.get_dummies(data[['age', 'sex']]), data.x53bp1)
print('OOB R^2: ', forest.oob_score_)

# +
# Can we develop a classfier to predict the transcriptomic-based subtyping (Wang et al., Cancer Cell, 2018) from protein expression data?
# Hint:
# step 1: go to http://gliovis.bioinfo.cnio.es/ and download the phenotypic data for the TCGA GBM cohort
# step 2: join the rppa protein expression table with the target class information extracted from gliovis
# step 3: train a classification random forest
# step 4: evaluate OOB accuracy
