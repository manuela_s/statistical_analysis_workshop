# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # An introduction to supervised and unsupervised multivariate statistical analysis
#
# ### Manuela Salvucci, RCSI
# #### 2019-10-09

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # What will we cover in this session?
#
# - Basic data exploration
# - A tour on un-supervised and supervised learning algorithms
# - Practise with the TCGA GBM dataset

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
#
# # How?
#
# - Mix of theory and hands-on practise using jupyter notebooks (python kernel)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # What's a jupyter notebook?
#
# ![jupyter_notebook_logo](images/jupyter_notebook_logo.png)
#
# - "jupyter" + "notebook"
#
#     - "jupyter": "acronym" from Julia + python + R (though many more languages are now supported)
#     - "notebook": collection of text, code, plots for an analysis (literate programming)
# - Jupyter project: https://jupyter.org/index.html
#
# - Gallery of interesting jupyter notebooks: https://github.com/jupyter/jupyter/wiki/A-gallery-of-interesting-Jupyter-Notebooks

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Anatomy of a jupyter notebook
# -

# * Notebook consists of a series of "cells"
# * Each cell can have text (markdown) or code (in our case, python).
# * A cell can be run by selecting it and pressing ![Run](images/jupyter_notebook_run_icon.png) button at the top (or pressing ctr-enter)
# * When the cell is run, the output is shown underneath: ![Cell](images/jupyter_notebook_cell_output.png)

# - Simple expressions

# + {"slideshow": {"slide_type": "-"}}
2+2

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# - Comments

# +
# Lines that start with a # are comments. 

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# - Store data in variables

# + {"slideshow": {"slide_type": "-"}}
a = 2 + 2
b = 1 + 1
a + b

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# - Cells can reference variables from prevoius cells (make sure to run the previous cells first!):

# + {"slideshow": {"slide_type": "-"}}
a - b

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Pandas tables
# -

# - Variables can also be used to store whole tables with data (using the pandas library), e.g. from a spreadsheet

# + {"slideshow": {"slide_type": "-"}}
import os
import pandas
countries = pandas.read_excel(os.path.join('data', 'countries.xls'), index_col='Country')
countries

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# - We can access individual columns from the table
# -

countries['Population']

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# - Perform operations on single columns
# -

countries['Population'].max()

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# - Or on multiple columns
# -

countries['Population'] / countries['Area']

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# - Making plots
# -

countries['Population'].plot(kind='bar');

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
#
# # Where can I find this material?
# - Presentation and code materials at https://bitbucket.org/manuela_s/statistical_analysis_workshop/
# - Binder version at http://tiny.cc/od51dz
# - Any question, comment, come chat to me or email me at manuelasalvucci@rcsi.ie
#
# # Exercise
# 1. Go to http://tiny.cc/od51dz
# 2. Click on "00_tools_and_setup_practise.py"
# 3. Play around
#
# Continue in 10 min.
