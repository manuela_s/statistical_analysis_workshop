[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fmanuela_s%2Fstatistical_analysis_workshop%2Fsrc%2Fmaster/master)

# Workshop
Presentation slides and notebooks for the workshop "An introduction to supervised and unsupervised multivariate statistical analysis​" held as part of the training course organized for PhD students of the [GLIOTRAIN programme](https://www.gliotrain.eu/) (Stuttgart, 2019-10-09).

## Materials
- Presentation slides:
    - [pdf format](https://manuelas.bitbucket.io/talk/statistical_analysis_workshop/notebook_slides.pdf)
    - [html format](https://manuelas.bitbucket.io/talk/statistical_analysis_workshop/notebook.slides.html)
- Jupyter notebooks with materials presented in the presentation slides:
    - [00_tools_and_setup.py](00_tools_and_setup.py)
    - [01_data_exploration.py](01_data_exploration.py)
    - [02_data_modelling.py](02_data_modelling.py)
    - [03_unsupervised_learning.py](03_unsupervised_learning.py)
    - [04_supervised_learning.py](04_supervised_learning.py)
    - [05_questions_and_hands_on.py](05_questions_and_hands_on.py)
- Jupyter notebook with practise exercises:
    - [00_tools_and_setup_practise.py](00_tools_and_setup_practise.py)
    - [06_TCGA_GBM_practise.py](06_TCGA_GBM_practise.py)
 
## Usage
Go to http://tiny.cc/od51dz to start exploring the notebooks (give binder a little time to load, no software installation required)

## Contact
For further information, please get in touch: manuelasalvucci@rcsi.ie
