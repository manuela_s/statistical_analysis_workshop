# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Unsupervised learning

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # What is *un*-supervised learning?
#
# - Learn a model from an un-labelled dataset
# - Model can identify patterns/structures inherent in the data
#     - similar samples (cell lines, animal models, patients) can be grouped together
#     - anomaly detection (i.e. rare phenotype)
#
# ![unsupervised_learning](images/unsupervised_learning.png)Image from https://medium.com/@deepanshugaur1998/scikit-learn-beginners-part-3-6fb05798acb1

# + {"slideshow": {"slide_type": "skip"}}
import os
import numpy
import pandas
import scipy.cluster.hierarchy
import sklearn.cluster
import sklearn.datasets
import seaborn
import matplotlib.pyplot

seaborn.set_context('notebook', font_scale=1.1, rc={'lines.linewidth': 2})

# + {"slideshow": {"slide_type": "skip"}}
iris = sklearn.datasets.load_iris()
features = pandas.DataFrame(iris.data, columns=iris.feature_names)
target = pandas.Series(pandas.Categorical.from_codes(iris.target, iris.target_names), name='species')
iris = pandas.concat([features, target], axis=1)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Clustering
#
# - From Wikipedia: *"Cluster analysis or clustering is the task of grouping a set of objects in such a way that objects in the same group (called a cluster) are more similar (in some sense) to each other than to those in other groups (clusters)."*
#
# - Several algorithms available:
#     - centroid-based (k-means)
#     - connectivity-based (hierarchical)
#     - density-based (DBSCAN, OPTICS, ...)
#     - ....

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Clustering
#
# ![sklearn_clustering_methods](images/sklearn_clustering_methods.png)
#
# - a table summaring use-case for each approach available at https://scikit-learn.org/stable/modules/clustering.html#clustering

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # K-means clustering applied to the iris dataset

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
seaborn.scatterplot(x='sepal length (cm)', y='sepal width (cm)', color='k', data=iris);


# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # K-means clustering applied to the iris dataset

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
def plot_kmeans(n_clusters, ax):
        kmeans = sklearn.cluster.KMeans(n_clusters=n_clusters)
        clusters = kmeans.fit_predict(iris[['sepal length (cm)', 'sepal width (cm)']])
        seaborn.scatterplot(x='sepal length (cm)', y='sepal width (cm)',
                            hue=clusters, palette='Set1', data=iris, ax=ax, legend=False)
        seaborn.scatterplot(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1],
                            hue=range(n_clusters), palette='Set1', marker='X', s=1000, legend=False, ax=ax)
        ax.set_title('n={} clusters'.format(n_clusters))
        return kmeans

fig, axes = matplotlib.pyplot.subplots(nrows=1, ncols=4, figsize=(24, 8))
kmeans = [plot_kmeans(i, ax) for (i, ax) in enumerate(axes, start=2)]

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # How many clusters? Elbow rule

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
matplotlib.pyplot.plot([2,3,4,5], [k.inertia_ for k in kmeans], '-ko', markersize=20)
ax = matplotlib.pyplot.gca()
ax.set_xlim(1.9, 5.1)
ax.set_xlabel('N clusters')
ax.set_ylabel('Inertia (sum of squared distances of samples to their closest cluster centre)')
fig = matplotlib.pyplot.gcf()
fig.set_figwidth(8)
fig.set_figheight(8)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Hierarchical clustering applied to the iris dataset
#

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
ac = sklearn.cluster.AgglomerativeClustering(n_clusters=2, affinity='euclidean', linkage='ward')
clusters = ac.fit_predict(iris[['sepal length (cm)', 'sepal width (cm)']])
seaborn.scatterplot(x='sepal length (cm)', y='sepal width (cm)',
                    hue=clusters, palette='Set1', data=iris, legend=False);

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Hierarchical clustering - a more familiar view?

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
linkage = scipy.cluster.hierarchy.linkage(iris[['sepal length (cm)', 'sepal width (cm)']], 'ward', optimal_ordering=True)
scipy.cluster.hierarchy.dendrogram(linkage, p=4, truncate_mode='lastp');

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Without clustering 

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
seaborn.heatmap(iris.iloc[:, 0:2], cmap='Blues')
ax = matplotlib.pyplot.gca()
ax.set_xlabel('Features')
ax.set_ylabel('Observations')
fig = matplotlib.pyplot.gcf()
fig.set_figwidth(8)
fig.set_figheight(8)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # With clustering 

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
seaborn.clustermap(iris.iloc[:, 0:2], method='ward', metric='euclidean', cmap='Blues')
fig = matplotlib.pyplot.gcf()
fig.set_figwidth(8)
fig.set_figheight(8)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Consensus clustering (aka ensemble clustering)
#
# - From Wikipedia: *" a number of different (input) clusterings have been obtained for a particular dataset and it is desired to find a single (consensus) clustering which is a better fit in some sense than the existing clusterings."*
# - Clustering algorithm is run multiple times --> assessment of number of clusters and cluster stability
# - Useful readings:
#     - Consensus Clustering: A Resampling-Based Method for Class Discovery and Visualization of Gene Expression Microarray Data (https://link.springer.com/article/10.1023/A:1023949509487)
#     - Bioconductor (R) Consensus Cluster package (https://bioconductor.org/packages/release/bioc/vignettes/ConsensusClusterPlus/inst/doc/ConsensusClusterPlus.pdf)
#     - Critical limitations of consensus clustering in class discovery(https://www.ncbi.nlm.nih.gov/pubmed/25158761)
#     - Avoiding common pitfalls when clustering biological data (https://www.ncbi.nlm.nih.gov/pubmed/27303057)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Clustering is typically combined with other covariates to provide an integrated view
#
# - "Cars" classicla dataset
#
# ![cars](https://bitbucket.org/manuela_s/hcp/raw/fbc0ac642e99b38263e99c456108a8c9ff829c3a/%2Bexamples/figures/cars.png) Image from http://doi.org/10.5281/zenodo.3242593

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Clustering is typically combined with phenotypic and other molecular data to provide an integrated view
# - TCGA cutanous melanoma dataset
# ![tcga_melanoma](images/tcga_cutaneous_melanoma_cancer.png) Image from http://doi.org/10.5281/zenodo.3242593

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # "Famous" clustering from the GBM realm
# - Transcriptomic-based subtyping (from bulk) tissue:
# ![verhaak_subtypes](images/verhaak_subtypes.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Consensus clustering
#
# - Let's explore the Spotify dataset with this app http://tiny.cc/w196dz (developed by @Ljfernando, https://towardsdatascience.com/consensus-clustering-f5d25c98eaf2)
# (5 min)
