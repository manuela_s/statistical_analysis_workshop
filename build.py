#!/usr/bin/env python
import glob
import os
import pathlib
import subprocess
import sys
import tempfile


def make_notebook(input, notebook):
    subprocess.run(['jupytext', '--to', 'notebook', '--execute', '-o', notebook, input], check=True)


def merge_notebooks(notebooks, merged_notebook):
    subprocess.run(['nbmerge', '-o', merged_notebook] + notebooks, check=True)


def convert_to_presentation(notebook, output):
    subprocess.run(['jupyter', 'nbconvert', '--to', 'slides', '--output', output,
                    '--output-dir', '.',
                    '--TagRemovePreprocessor.remove_input_tags={"hide_input"}',
                    notebook], check=True)


def add_html_base_tag(filename, base_url):
    path = pathlib.Path(filename)
    text = path.read_text()
    text = text.replace('<head>', f'<head>\n<base href="{base_url}">', 1)
    path.write_text(text)


def make_presentation(inputs, output):
    """
    Make presentation from multiple .py files

    inputs: list of input files
    output: name of presentation to generate.
    """
    with tempfile.TemporaryDirectory() as tmpdirname:
        notebooks = []
        for input in inputs:
            notebook = os.path.join(tmpdirname, '{}.ipynb'.format(os.path.splitext(input)[0]))
            make_notebook(input, notebook)
            notebooks.append(notebook)

        merged_notebook = os.path.join(tmpdirname, 'merged_notebook.ipynb')
        merge_notebooks(notebooks, merged_notebook)

        convert_to_presentation(merged_notebook, output)
        add_html_base_tag('notebook.slides.html',
                          'https://bitbucket.org/manuela_s/statistical_analysis_workshop/raw/gliotrain_workshop/')


if __name__ == '__main__':
    if len(sys.argv) > 1:
        files = sys.argv[1:]
    else:
        files = sorted([ x for x in glob.glob('0?_*.py') if not x.endswith('practise.py')])

    make_presentation(files, 'notebook')
