# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"slideshow": {"slide_type": "-"}, "cell_type": "markdown"}
# # An introduction to supervised and unsupervised multivariate statistical analysis
#
# ### Manuela Salvucci, RCSI
# #### 2019-10-09
# #### Exercise 0
# -

# Simple expressions.
# Use the "Run" icon (or press ctrl-enter) to run each cell.
#
# Hint: Try + - / and *

# + {"slideshow": {"slide_type": "-"}}
2+2
# -

# Storing results in variables

# + {"slideshow": {"slide_type": "-"}}
a = 2 + 2
b = 1 + 1
a + b

# + {"slideshow": {"slide_type": "-"}, "cell_type": "markdown"}
# Cells can reference variables from prevoius cells (make sure to run the previous cells first!):

# + {"slideshow": {"slide_type": "-"}}
a - b

# + {"slideshow": {"slide_type": "-"}, "cell_type": "markdown"}
# Pandas tables
# -

# Variables can also be used to store whole tables with data (using the pandas library), e.g. from a spreadsheet

# + {"slideshow": {"slide_type": "-"}}
import os
import pandas
countries = pandas.read_excel(os.path.join('data', 'countries.xls'), index_col='Country')
countries

# + {"slideshow": {"slide_type": "-"}, "cell_type": "markdown"}
# We can access individual columns from the table.
#
# Hint: Try the other column names from the table above.
# -

countries['Area']

# + {"slideshow": {"slide_type": "-"}, "cell_type": "markdown"}
# Perform operations on single columns.
#
# Hint: Try max, min, sum, mean or median
# -

countries['Area'].max()

# + {"slideshow": {"slide_type": "-"}, "cell_type": "markdown"}
# Or on multiple columns
# -

countries['Population'] / countries['Area']

# Making plots
#
# Hint: Try a different column or kind='line'

countries['Population'].plot(kind='bar');
