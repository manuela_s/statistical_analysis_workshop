# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Data exploration
#
# - Exploratory data analysis (EDA) is a crucial part of any research project
#     - quality control (missing data, outlier detection, ...)
#     - descriptive statistics
#     - visualization
#     - insights to guide downstream analysis
#
# ![data_exploration_cartoon](images/data_exploration_cartoon.png) Image from http://www.codeheroku.com/post.html?name=Introduction%20to%20Exploratory%20Data%20Analysis%20(EDA)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Data exploration
#
# - We will use "classical" datasets:
#     - clean data
#     - QC-ed data
#     - no missing values
#     - balanced cases

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # The iris dataset
# - The iris dataset (https://en.wikipedia.org/wiki/Iris_flower_data_set) was first described by Fisher in 1936
# - It includes measurements for 4 flower characteristics (length and width for petals and sepals) for 150 flowers from 3 different iris species
#
# ![iris_species](images/iris_species.png)

# + {"slideshow": {"slide_type": "skip"}, "tags": ["input_hide"]}
import os
import numpy
import pandas

import sklearn.datasets

import seaborn
import matplotlib.pyplot

seaborn.set_context('notebook', font_scale=1.5, rc={'lines.linewidth': 2})

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's explore the iris dataset
#
# - To load the data, we do:

# + {"slideshow": {"slide_type": "-"}, "tags": ["input_hide"]}
iris = sklearn.datasets.load_iris()
features = pandas.DataFrame(iris.data, columns=iris.feature_names)
target = pandas.Series(pandas.Categorical.from_codes(iris.target, iris.target_names), name='species')
iris = pandas.concat([features, target], axis=1)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's explore the iris dataset
#
# - Use .head() to show the first few samples.

# + {"tags": ["input_hide"], "slideshow": {"slide_type": "-"}}
iris.head()
# -

# ![iris_petal_sepal](images/iris_petal_sepal.png) Image from https://www.integratedots.com/determine-number-of-iris-species-with-k-means/

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's explore the iris dataset
#
# - Each row represents 1 observation (i.e. 1 sample/flower)

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
iris.head().style.apply(lambda x: ['background: lightblue' if x.name == 0 else '' for i in x], axis=1)

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# - Each column represents 1 variable(i.e. 1 type of attribute/characteristic/feature)

# + {"tags": ["hide_input"]}
iris.head().style.apply(lambda x: ['background: lightblue' if x.name == 'sepal length (cm)' else '' for i in x])

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's explore the iris dataset
#
# - Here, "species" is our phenotype of interest which we refer to as "target class" or "target"

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
iris.head().style.apply(lambda x: ['background: red' if x.name == 'species' else 'background: lightblue' for i in x])

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# - Summary statistics for the measurements

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
iris.describe().style.apply(lambda x: ['background: lightblue' if x.name == '50%' else '' for i in x], axis=1)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's explore the iris dataset
#
# - Breakdown of each species in the dataset

# + {"slideshow": {"slide_type": "-"}}
iris.species.value_counts()

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's get plotting the iris dataset
#
# - Sepal length by species

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
fig, axes = matplotlib.pyplot.subplots(nrows=1, ncols=4, figsize=(24, 8))
# Dot plot with no grouping variable
seaborn.swarmplot(y='sepal length (cm)', color='k', data=iris, ax=axes[0])
# Dot plot grouped by species
seaborn.swarmplot(x='species', y='sepal length (cm)', dodge=True, data=iris.reset_index(), ax=axes[1])
# Boxplot grouped by species
seaborn.boxplot(x='species', y='sepal length (cm)', dodge=True, data=iris.reset_index(), ax=axes[2])
# Violin plot grouped by species
seaborn.violinplot(x='species', y='sepal length (cm)', dodge=True, data=iris.reset_index(), ax=axes[3]);

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's get plotting the iris dataset

# + {"slideshow": {"slide_type": "-"}}
fig, axes = matplotlib.pyplot.subplots(nrows=1, ncols=4, figsize=(24, 8))
# Dot plot with no grouping variable
seaborn.swarmplot(y='sepal length (cm)', color='k', data=iris, ax=axes[0])
# Dot plot grouped by species
seaborn.swarmplot(x='species', y='sepal length (cm)', dodge=True, data=iris.reset_index(), ax=axes[1])
# Boxplot grouped by species
seaborn.boxplot(x='species', y='sepal length (cm)', dodge=True, data=iris.reset_index(), ax=axes[2])
# Violin plot grouped by species
seaborn.violinplot(x='species', y='sepal length (cm)', dodge=True, data=iris.reset_index(), ax=axes[3]);

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's get plotting the iris dataset
#
# - All features grouped by species

# + {"slideshow": {"slide_type": "subslide"}}
iris.loc[[6, 92, 140]]

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# - Let's stack the table and add a grouping variable

# + {"slideshow": {"slide_type": "subslide"}}
iris_tall = iris.set_index('species', append=True).stack().to_frame('value')
iris_tall.index.names = ['id', 'species', 'feature']
iris_tall.loc[[6, 92, 140]]

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's get plotting the iris dataset
#
# - All features grouped by species

# + {"slideshow": {"slide_type": "-"}}
seaborn.catplot(x='species', y='value', col='feature', sharey=False, data=iris_tall.reset_index());

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's get plotting the iris dataset
#
# - Bivariate analysis

# + {"slideshow": {"slide_type": "-"}}
seaborn.jointplot(x='sepal length (cm)', y='petal width (cm)', kind='reg', data=iris);

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's get plotting the iris dataset
#
# - Bivariate analysis

# + {"slideshow": {"slide_type": "-"}}
seaborn.scatterplot(x='sepal length (cm)', y='petal width (cm)', hue='species', data=iris);

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's get plotting the iris dataset
#
# - Bivariate analysis

# + {"slideshow": {"slide_type": "-"}}
g = seaborn.pairplot(iris, hue='species');

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's get plotting the iris dataset
#
# - Correlation analysis

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
corr = iris.drop(columns=['species']).corr()
mask = numpy.zeros_like(corr)
mask[numpy.triu_indices_from(mask)] = True
seaborn.heatmap(corr,
                mask=mask,
                square=True,
                cmap='RdBu',
                vmin=-1,
                vmax=1,
                linewidth=1);


# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Though correlation does not imply causation....
#
# <img src="images/xkcd_correlation.png" align="centre" />

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Though correlation does not imply causation....
#
# ![xkcd_correlation](images/spurius_correlation1.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # The Anscombe quartet: same correlation, different underlying data....
#
# - https://en.wikipedia.org/wiki/Anscombe%27s_quartet
#
#

# + {"tags": ["hide_input"]}
# Code from https://matplotlib.org/examples/pylab_examples/anscombe.html

x = [10, 8, 13, 9, 11, 14, 6, 4, 12, 7, 5]
y1 = [8.04, 6.95, 7.58, 8.81, 8.33, 9.96, 7.24, 4.26, 10.84, 4.82, 5.68]
y2 = [9.14, 8.14, 8.74, 8.77, 9.26, 8.10, 6.13, 3.10, 9.13, 7.26, 4.74]
y3 = [7.46, 6.77, 12.74, 7.11, 7.81, 8.84, 6.08, 5.39, 8.15, 6.42, 5.73]
x4 = [8, 8, 8, 8, 8, 8, 8, 19, 8, 8, 8]
y4 = [6.58, 5.76, 7.71, 8.84, 8.47, 7.04, 5.25, 12.50, 5.56, 7.91, 6.89]


def fit(x):
    return 3 + 0.5 * x


fig, axs = matplotlib.pyplot.subplots(2, 2, sharex=True, sharey=True)
axs[0, 0].set(xlim=(0, 20), ylim=(2, 14))
axs[0, 0].set(xticks=(0, 10, 20), yticks=(4, 8, 12))

xfit = numpy.array([numpy.min(x), numpy.max(x)])
axs[0, 0].plot(x, y1, 'ks', xfit, fit(xfit), 'r-', lw=2)
axs[0, 1].plot(x, y2, 'ks', xfit, fit(xfit), 'r-', lw=2)
axs[1, 0].plot(x, y3, 'ks', xfit, fit(xfit), 'r-', lw=2)
xfit = numpy.array([numpy.min(x4), numpy.max(x4)])
axs[1, 1].plot(x4, y4, 'ks', xfit, fit(xfit), 'r-', lw=2)

for ax, label in zip(axs.flat, ['I', 'II', 'III', 'IV']):
    ax.label_outer()
    ax.text(3, 12, label, fontsize=20)

# verify the stats
pairs = (x, y1), (x, y2), (x, y3), (x4, y4)
for x, y in pairs:
    print('mean=%1.2f, std=%1.2f, r=%1.2f' % (numpy.mean(y), numpy.std(y),
          numpy.corrcoef(x, y)[0][1]))


# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Notes
#
# - When in doubt, make some more plots...
# - Useful tips on data visualization:
#     - http://blogs.nature.com/methagora/2013/07/data-visualization-points-of-view.html
#     - https://www.ncbi.nlm.nih.gov/pubmed/25901488
#     - https://serialmentor.com/dataviz/index.html
# - Software for data visualization:
#     - (A few) of python's plotting libraries:
#        - matplotlib
#        - seaborn
#        - ggplot
#        - plotly
#        - bokeh
#        - ggplot   
#     - Other libraries/packages/toolboxes:
#        - ggplot2 (R)
#        - GRAMM (MATLAB)
# - GUI-based tools:
#     - http://gliovis.bioinfo.cnio.es/
#     - http://www.instantclue.uni-koeln.de/ (from https://www.ncbi.nlm.nih.gov/pubmed/30140043)
