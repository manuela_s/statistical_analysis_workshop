# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Data modelling
#
# **Herbert Simon**: “Learning is any process by which a system improves performance from experience.”

# + {"slideshow": {"slide_type": "skip"}}
import os
import numpy
import pandas

import keras.datasets
import sklearn.datasets

import sklearn.decomposition
import sklearn.manifold
import umap

import seaborn
import matplotlib.pyplot

seaborn.set_context('notebook', font_scale=1.1, rc={'lines.linewidth': 2})

# + {"slideshow": {"slide_type": "skip"}}
iris = sklearn.datasets.load_iris()
features = pandas.DataFrame(iris.data, columns=iris.feature_names)
target = pandas.Series(pandas.Categorical.from_codes(iris.target, iris.target_names), name='species')
iris = pandas.concat([features, target], axis=1)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Data, data & more data needed…
#
#
#
# ![data_data_more_data](images/data_data_more_data.png) Image from http://web.orionhealth.com/rs/981-HEV-035/images/Introduction_To_Machine_Learning_US.pdf
#     

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Data sources for cancer and GBM specifically...
#
# - TCGA datasets
# - GlioVis (http://gliovis.bioinfo.cnio.es/)
# - https://portals.broadinstitute.org/ccle

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Fat data rather than big data....
#
# - In biology, we typically have a lot of data for a relatively small number of samples (cell lines, animal models, patients)
# - The curse of dimensionality (10-100s of rows with samples x 100K features)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Dimensionality reduction
#
# - Techniques designed to visualize high-dimensional data in in lower dimensions, typically 2D:
#     - linear transformation:
#         - Principal Component Analysis (**PCA**)
#     - non-linear transformation:
#         - t-distributed stochastic neighbor embedding (**t-SNE**, https://lvdmaaten.github.io/tsne/)
#         - UMAP (**Uniform Manifold Approximation and Projection**, https://github.com/lmcinnes/umap)
# - Aid in exploratory data analysis and modelling
# - Useful literature:
#     - https://www.ncbi.nlm.nih.gov/pubmed/30531897
#     - https://www.biorxiv.org/content/early/2019/05/20/453449.full.pdf
#     - https://www.biorxiv.org/content/biorxiv/early/2019/02/15/549659.full.pdf

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Principal component analysis (PCA) applied to the iris dataset

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
pca = sklearn.decomposition.PCA()
iris_pca = pca.fit_transform(iris.drop(columns=['species']))

fig, axes = matplotlib.pyplot.subplots(1, 2, sharey=False, figsize=(14, 8))
seaborn.barplot(x=[1,2,3,4], y=pca.explained_variance_ratio_ * 100, color='k', ax=axes[0])
axes[0].set_xlabel('Principal components (PCs)')
axes[0].set_ylabel('Explained variance [%]')
axes[0].set_ylim(0, 100)

seaborn.scatterplot(x=iris_pca[:,0], y=iris_pca[:,1], hue=iris.species, ax=axes[1])
axes[1].set_xlabel('PC1')
axes[1].set_ylabel('PC2');

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Let's explore PCA and alternative embeddings using the fashion MNIST dataset
#
# - Dataset compiled and released by Zelando for 70K images spanning 10 classes of clothing items
# ![fashion_mnist](images/fashion_mnist.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # PCA vs. t-SNE vs. UMAP

# + {"slideshow": {"slide_type": "skip"}}
# Load fashion MNIST dataset
(fashion_x, fashion_y), _ = keras.datasets.fashion_mnist.load_data()
fashion_x = fashion_x.reshape([60000, -1])

fashion_x = fashion_x[:1000]
fashion_y = fashion_y[:1000]

clothing_classes = [
    'T-shirt/top',
    'Trouser',
    'Pullover',
    'Dress',
    'Coat',
    'Sandal',
    'Shirt',
    'Sneaker',
    'Bag',
    'Ankle boot']

# PCA
fashion_embeddings_pca = sklearn.decomposition.PCA(n_components=2).fit_transform(fashion_x)

# t-SNE
fashion_embeddings_tsne = sklearn.manifold.TSNE().fit_transform(fashion_x)

# UMAP
fashion_embeddings_umap = umap.UMAP(n_neighbors=5).fit_transform(fashion_x)

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
norm = matplotlib.colors.Normalize(vmin=0.5, vmax=9.5, clip=False)
cmap = 'Spectral'


def embedding_scatterplot(ax, embeddings, title):
    seaborn.scatterplot(x=embeddings[:,0], y=embeddings[:,1],
                    hue=fashion_y,
                    palette=cmap,
                    norm=norm,
                    size=2,
                    legend=None,
                    ax=ax)
    ax.set_xlabel('1st component')
    ax.set_ylabel('2nd component')
    ax.set_title(title)

    
fig, axes = matplotlib.pyplot.subplots(nrows=1, ncols=3, figsize=(14, 10))

# PCA
embedding_scatterplot(axes[0], fashion_embeddings_pca, 'PCA')

# t-SNE
embedding_scatterplot(axes[1], fashion_embeddings_tsne, 't-SNE')

# UMAP
embedding_scatterplot(axes[2], fashion_embeddings_umap, 'UMAP')

# Set colorbar and map to clothing classes
cbar = fig.colorbar(matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap), boundaries=numpy.arange(11)-0.5)
cbar.set_ticks(numpy.arange(10))
cbar.set_ticklabels(clothing_classes)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Autoencoders
# - Auto-encoders (artificial neural networks) to extract meta-features
# - While the dimensionality reduction techniques we have seen so far are unsupervised, autoencoders are a special class of supervised learning
#
# ![autoencoder](images/autoencoder_cartoon.png) Image from https://towardsdatascience.com/deep-autoencoders-using-tensorflow-c68f075fd1a3

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Supervised vs. unsupervised learning algorithms
#
# ![supervised_vs_unsupervised](images/supervised_vs_unsupervised.jpg) Image from https://blog.westerndigital.com/machine-learning-pipeline-object-storage/

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Supervised vs. unsupervised learning algorithms
#
# ![supervised_vs_unsupervised_cartoon2](images/supervised_vs_unsupervised_cartoon2.png) Image from https://mapr.com/blog/apache-spark-machine-learning-tutorial/​
