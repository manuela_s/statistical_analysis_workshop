# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Supervised learning

# + {"slideshow": {"slide_type": "-"}, "cell_type": "markdown"}
# - classification vs regression
# - regression
#   - case: linear regression
# - classification
#   - case: decision trees
#   - case: random forest
#   - metrics of performance
#     - confusion matrix
# - other models (not covered)
#   - variation of forest like xgboots, extreme trees
#   - support vector machine (SVM)
#   - (deep) neural networks
#   - model stacking

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # What is supervised learning?
#
# - Learn a model from a labelled dataset
# - Model can predict label on previously unseen samples​
#
# ![supervised_learning](images/supervised_learning.png)Image from https://medium.com/@deepanshugaur1998/scikit-learn-beginners-part-2-ca78a51803a8

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Generalization
#
# How do we know how well what we've learned from one dataset may apply to other datasets?
#
# Simplest technique:
# 1. Split our dataset in a "training set" and "testing set" (and sometimes a "validation set").
# 2. Learn feature -> label mapping from "training set" only.
# 3. Use model to guess labels for "testing set". Check how well it matches the known labels.
#
# This gives us an estimate for how well the model can predict labels for samples it has not seen. 
#
# **Cost**: Requires bigger dataset. Other techniques (like cross-validation) reduce the overhead. For Random Forests there is a "trick" that allows us to train on all the data and yet estimate performance on unseen data.

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Generalization
#
# ![supervised_vs_unsupervised_training_testing](images/supervised_vs_unsupervised_training_testing.png) Image from https://blogs.nvidia.com/blog/2018/08/02/supervised-unsupervised-learning/

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Classification vs. regression
# - In classification the output is a class or label (discrete), while in regression the output is number (continous) 
# - Classification can choose between 2 classes (binary) or more (multi-class)
#
# ![classification_vs_regression](images/classification_vs_regression.jpeg)Image from https://medium.com/@ali_88273/regression-vs-classification-87c224350d69

# + {"slideshow": {"slide_type": "skip"}}
import os
import numpy
import pandas

import sklearn.metrics
import sklearn.datasets
import sklearn.linear_model
import sklearn.tree
import sklearn.ensemble
import sklearn.model_selection

import seaborn
import matplotlib.pyplot

seaborn.set_context('notebook', font_scale=1.1, rc={'lines.linewidth': 2, 'figure.figsize':(20, 8)})

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Linear regression with the Boston housing dataset

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
boston = sklearn.datasets.load_boston()

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
print(boston['DESCR'])

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "skip"}}
features = pandas.DataFrame(boston.data, columns=boston.feature_names)
target = pandas.Series(boston.target, name='price')
boston = pandas.concat([features, target], axis=1)
boston_training, boston_testing = sklearn.model_selection.train_test_split(boston)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Linear regression with the Boston housing dataset

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
boston.head().style.apply(lambda x: ['background: red' if x.name == 'price' else 'background: lightblue' for i in x])

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "fragment"}}
fig = matplotlib.pyplot.figure(figsize=(20, 10))
ax = seaborn.distplot(boston_training.price, bins=100)
ax.set_xlabel('House price in Boston [K $]')
ax.set_ylabel('Frequency');

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's predict the price from LSTAT
#
# - LSTAT: Percentage of lower status of the population

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
seaborn.jointplot(x='LSTAT', y='price', kind='reg', data=boston_training);

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Let's predict the price from LSTAT
#
# - LSTAT: Percentage of lower status of the population

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
linear_model = sklearn.linear_model.LinearRegression()
linear_model.fit(boston_training.drop(columns=['price']), boston_training.price);

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
# Model performance in the training set
predictions_training = linear_model.predict(boston_training.drop(columns=['price']))

training_rmse = (numpy.sqrt(sklearn.metrics.mean_squared_error(boston_training.price, predictions_training)))
training_r2 = sklearn.metrics.r2_score(boston_training.price, predictions_training)

print("Model performance in the training set:")
print('Root mean-squared error (RMSE) = {:.2f}'.format(training_rmse))
print('Coefficient of determination (R2) = {:.2f}'.format(training_r2))

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "fragment"}}
# Model performance in the testing set
predictions_testing = linear_model.predict(boston_testing.drop(columns=['price']))

testing_rmse = (numpy.sqrt(sklearn.metrics.mean_squared_error(boston_testing.price, predictions_testing)))
testing_r2 = sklearn.metrics.r2_score(boston_testing.price, predictions_testing)

print("Model performance in the testing set:")
print('Root mean-squared error (RMSE) = {:.2f}'.format(testing_rmse))
print('Coefficient of determination (R2) = {:.2f}'.format(testing_r2))

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Classification with IRIS dataset
# ## Predict species from petal length/width

# + {"slideshow": {"slide_type": "skip"}, "tags": ["hide_input"]}
iris = sklearn.datasets.load_iris()
features = pandas.DataFrame(iris.data, columns=iris.feature_names)
target = pandas.Series(pandas.Categorical.from_codes(iris.target, iris.target_names), name='species')
iris = pandas.concat([features, target], axis=1)
iris_training, iris_testing = sklearn.model_selection.train_test_split(iris)

# + {"tags": ["hide_input"]}
iris_tree = sklearn.tree.DecisionTreeClassifier(min_impurity_decrease=0.01)
iris_tree.fit(iris_training[['petal length (cm)', 'petal width (cm)']], iris_training.species);

# + {"tags": ["hide_input"]}
fig, axes = matplotlib.pyplot.subplots(nrows=1, ncols=2, figsize=(20,8))
seaborn.scatterplot(x='petal length (cm)', y='petal width (cm)', hue='species', palette='Set1', s=100, ax=axes[0], data=iris)
matplotlib.pyplot.axvline(0, 2)
sklearn.tree.plot_tree(iris_tree, feature_names=['petal length (cm)', 'petal width (cm)'], class_names=iris_tree.classes_, filled=True, rounded=True, proportion=True, ax=axes[1]);
xlim = axes[0].get_xlim()
ylim = axes[0].get_ylim()

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Visualize what the classifier has learned

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
x, y = numpy.meshgrid(numpy.linspace(*xlim, 100), numpy.linspace(*ylim, 100))
predictions = pandas.Series(
    pandas.Categorical(iris_tree.predict(numpy.hstack([x.reshape(-1, 1), y.reshape(-1, 1)])), iris_tree.classes_))

fig = matplotlib.pyplot.figure(figsize=(14, 8))
matplotlib.pyplot.contourf(x, y, predictions.cat.codes.values.reshape(x.shape), colors=matplotlib.cm.Set1.colors, levels=2, alpha=0.1)
seaborn.scatterplot(x='petal length (cm)', y='petal width (cm)', hue='species', s=200, edgecolor='k', palette='Set1', data=iris);

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Decision tree performance (training set)

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
iris_training_predictions = iris_tree.predict(iris_training[['petal length (cm)', 'petal width (cm)']])
print(sklearn.metrics.classification_report(iris_training.species, iris_training_predictions))

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# # Decision tree performance (testing set)

# + {"tags": ["hide_input"]}
iris_testing_predictions = iris_tree.predict(iris_testing[['petal length (cm)', 'petal width (cm)']])
print(sklearn.metrics.classification_report(iris_testing.species, iris_testing_predictions))

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Random Forest
#
# ![random_forest_cartoon](images/random_forest_cartoon.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Classification Random Forest
#
# - Train many decision trees.
# - Each tree is trained on slightly different data; different subsets of samples and of features.
# - To make a classification, we use all trees and the class with the most votes wins.

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# **Why**: Training on subsets of samples and features help avoid overfitting (learning a model that reflects the training data _too_ closely). Using multiple trees help get high accuracy on unseen data.
# ![overfitting](images/overfitting_21.png)
# (from https://www.geeksforgeeks.org/regularization-in-machine-learning/)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Classification Random Forest to predict species in the Iris dataset
# ## Confusion matrix
# ![confusion_matrix_cartoon](images/confusion_matrix_cartoon.png) Image from https://towardsdatascience.com/handling-imbalanced-datasets-in-machine-learning-7a0e84220f28

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Classification Random Forest to predict species in the Iris dataset
# ## Confusion matrix

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "skip"}}
rf = sklearn.ensemble.RandomForestClassifier(n_estimators=100, oob_score=True)
rf.fit(iris.drop(columns=['species']), iris.species);

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "skip"}}
oob_predictions = pandas.DataFrame(
    rf.oob_decision_function_, index=iris.index,
    columns=iris.species.cat.categories).idxmax(axis=1)

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
print('OOB Accuracy = {:.2f}'.format(sklearn.metrics.accuracy_score(iris.species, oob_predictions)))

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
matplotlib.pyplot.figure(figsize=(14, 6))
seaborn.heatmap(sklearn.metrics.confusion_matrix(iris.species, oob_predictions), annot=True, cmap='Blues', square=True)
ax = matplotlib.pyplot.gca()
ax.set_xlabel('Ground truth')
ax.set_ylabel('Predictions')
ax.set_xticklabels(rf.classes_)
ax.set_yticklabels(rf.classes_)
ax.set_ylim([0, 3]);

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Behind the scenes
# ## Trees inside the forest

# + {"tags": ["hide_input"]}
fig, axes = matplotlib.pyplot.subplots(nrows=1, ncols=2, figsize=(20,8))
sklearn.tree.plot_tree(rf.estimators_[0], feature_names=iris.columns[:4], class_names=iris_tree.classes_, 
                       filled=True, rounded=True, proportion=True, ax=axes[0]);
sklearn.tree.plot_tree(rf.estimators_[1], feature_names=iris.columns[:4], class_names=iris_tree.classes_, 
                       filled=True, rounded=True, proportion=True, ax=axes[1]);


# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Classification Random Forest to predict species in the Iris dataset
# ## Votes for individual trees in the forest

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
tree_predictions = numpy.vstack([tree.predict(iris_testing.drop(columns=['species'])) for tree in rf.estimators_]).T
matplotlib.pyplot.figure(figsize=(14, 6))
ax = seaborn.heatmap(tree_predictions, cmap='Set1', vmax=7, cbar_kws={'values': [0, 1, 2], 'ticks': [0,1,2]})
ax.set_xlabel('Classification decision trees')
ax.set_xlim(0, 100)
ax.xaxis.set_major_locator(matplotlib.ticker.AutoLocator())
ax.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax.set_ylabel('Observations');

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Random Forest as feature selection algorithms
#
# - Impurity-based variable importance
# - Permutation-based variable importance
# - SHAP (SHapley Additive exPlanations)-based variable importance
#

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # SHAP feature importance
#
# - Feature importance for the dataset
# ![boston_summary_noise](images/boston_summary_plot_bar.png) Image from https://github.com/slundberg/shap

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # SHAP feature importance
#
# - Feature importance for the dataset
# ![boston_summary_noise2](images/boston_summary_plot.png) Image from https://github.com/slundberg/shap

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # SHAP feature importance
#
# - Feature importance for specific dataset observations
# ![boston_singl_predictions](images/boston_single_prediction.png) Image from https://github.com/slundberg/shap

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Supervised learning
#
# - Classification vs. regression
# - Regression
#   - case: linear regression
# - Classification
#   - case: decision trees
#   - case: random forest
#   - metrics of performance
#     - confusion matrix
# - **Other models (not covered)**
#   - *Variation of tree-based approaches* like xgboots, extreme trees
#   - *Support vector machine (SVM)*: A hyperplane to separate the groups in high-dimensional space.
#   - *(deep) neural networks*: machine learning inspired by animal brains. Many layers of simple neurons that    
#     collectively can learn complex patterns.
#   - *Model stacking*: combining multiple learning algorithms for higher accuracy.
